# SendGrid Mailer 

## Technologies
- NodeJS 12.16.0
- HapiJS 19.1.1
- [SendGrid](https://sendgrid.com/)
- Google Cloud Platform

## Local Development Setup
- Clone this repository
- Install dependencies
```
$ cd af-mailer
$ npm install
```
- Generate a SENDGRID_API_KEY in [SendGrid](https://sendgrid.com/)
- Create *.env* file from *.env-tpl* and provide the SENDGRID_API_KEY.
- Set the SENDGRID_SENDER email and the NOTIFY email as well.
- Create [Dynamic Templates in SendGrid](https://sendgrid.com/dynamic_templates)
- Modify *appConfig.js* to configure the TEMPLATES ids.
- Run the server application
```
$ source .env
$ npm run start
```
- Test the mailer with curl
```
$ curl -i \
    -H "Accept: application/json" \
    -X POST -d '{"firstName":"Francis","subject":"Hello","email":"francisfueconcillo@icloud.com"}' \
    http://localhost:8081/mailer
```

## Deployment
- Create a GCP Project
- Enable App Engine

```
$ cd af-mailer
$ gcloud init  # create gcp configuration
$ gcloud app deploy
```


