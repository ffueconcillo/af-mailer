const appConfig = {
  RECAPTCHA: {
    ENABLE: true,
    MID_SCORE: 0.5,
    VERIFY_URL: 'https://www.google.com/recaptcha/api/siteverify',
  },
  SENDER_NAME: 'API First Solutions',
  TEMPLATES: {
    CONTACT_CONFIRMATION: 'd-83d1f3715ef848d2867e225054f7c66f',
    CONTACT_NOTIFICATION: 'd-d85a3304a877423cba533d3c22a9e8e8'
  },
}

module.exports = appConfig;