
const Hapi = require('@hapi/hapi');
const Joi = require('@hapi/joi');
const JoiPhoneNumber = Joi.extend(require('joi-phone-number'));
const SendGridMail = require('@sendgrid/mail');

const appConfig = require('./appConfig');
const validateCaptcha = require('./validateCaptcha')

const init = async () => {
  const server = Hapi.server({
    port: process.env.PORT || 8081,
    host: "0.0.0.0"
  });

  SendGridMail.setApiKey(process.env.SENDGRID_API_KEY);

  // const allowedDommain = '^(https?://(?:.+\.)?'+ process.env.ALLOWED_DOMAIN +'\.com(?::\d{1,5})?)$'

  let allowedDommains = [
    'http://' + process.env.ALLOWED_DOMAIN,
    'https://' + process.env.ALLOWED_DOMAIN,
    'https://www.' + process.env.ALLOWED_DOMAIN,
    'http://www.' + process.env.ALLOWED_DOMAIN,
  ];

  if (process.env.ENVIRONMENT === 'development') {
    allowedDommains = ['*'];
  }

  server.route({
    method: "POST",
    path: "/mailer",
    config: {
      cors: {
          origin: allowedDommains,  
          additionalHeaders: ['cache-control', 'x-requested-with', 'vary']
      }
    },
    handler: async (request, h) => {
      const {
        firstName,
        lastName,
        email,
        phone, 
        subject,
        topic,
        message,
        captchaToken,
      } = request.payload;

      let validators = {
        firstName: Joi.string().required(),
        lastName: Joi.string().allow('').allow(null).optional(),
        email: Joi.string().email().required(),
        phone: JoiPhoneNumber.string().allow('').allow(null).phoneNumber().optional(), 
        subject: Joi.string().required(),
        topic: Joi.string().allow('').allow(null).optional(),
        message: Joi.string().required(),
      };

      const validate = Joi.object(validators).validate({ 
        firstName,
        lastName,
        email,
        phone, 
        subject,
        topic,
        message,
      });  
      
      if (validate.error) {
        return { 
          success: false, 
          error: {
            key: validate.error.details[0].context.key,
            message: validate.error.details[0].message
          }
        };   
      }

      if (appConfig.RECAPTCHA.ENABLE) {
        const isCaptchaValid = await validateCaptcha(captchaToken);
        
        if (!isCaptchaValid) {
          return { 
            success: false, 
            error: {
              key: captchaToken,
              message: 'Captcha validation failed. Please refresh this page.',
            }
          };
        } 
      }

      const msg = {
        to: email,
        from:  { 
          email: process.env.SENDGRID_SENDER, 
          name: appConfig.SENDER_NAME,
        },
        templateId: appConfig.TEMPLATES.CONTACT_CONFIRMATION,
        dynamic_template_data: {
          firstName,
          subject,
        },
      };

      const notifyMsg = {
        to: process.env.NOTIFY,
        from:  { 
          email: process.env.SENDGRID_SENDER, 
          name: appConfig.SENDER_NAME,
        },
        templateId: appConfig.TEMPLATES.CONTACT_NOTIFICATION,
        dynamic_template_data: {
          firstName,
          lastName,
          email,
          phone, 
          subject,
          topic,
          message,
        },
      };
      
      // Send confirmation message to client
      SendGridMail.send(msg)
          .catch(error => {
            console.error(`SendGrid Error: ${error.code} :${error.message}`);
          });

      // Send notification message 
      SendGridMail.send(notifyMsg)
        .catch(error => {
          console.error(`SendGrid Error: ${error.code} :${error.message}`);
      });

      return { success: true };
    }
  });

  await server.start();
  console.log("Server running on %s", server.info.uri);
};

process.on("unhandledRejection", err => {
  console.log(`Hapi Error: ${err}`);
  process.exit(1);
});

init();