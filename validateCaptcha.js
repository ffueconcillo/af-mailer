

const appConfig = require('./appConfig');
const fetch = require('node-fetch');

const validateCaptcha = async(token) => {
  if (token === '') { return false };

  const result = await fetch(appConfig.RECAPTCHA.VERIFY_URL, {
    method: 'POST',
    headers: { "Content-Type": "application/x-www-form-urlencoded" },
    body: `secret=${process.env.RECAPTCHA_SECRET}&response=${token}`,
  })
  .then(res => res.json())
  .then(resJson => {
    return resJson.success && resJson.score > appConfig.RECAPTCHA.MID_SCORE
  });

  return result;
};

module.exports = validateCaptcha;